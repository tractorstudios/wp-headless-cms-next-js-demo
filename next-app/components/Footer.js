import { getPathFromUrl } from '@/lib/utils'
import { useRouter } from 'next/router'
import Link from 'next/link'

export default ({ links }) => {
  const router = useRouter()

  const linkUrl = url => (
    getPathFromUrl(url)
  )

  return (
    <div className="flex p-4 bg-gray-700">
      {links && (<ul className="text-sm text-gray-200 flex">
        {links.map(link => (
          <li key={link.ID} className="mr-4">
            <span className="font-bold"><Link href={getPathFromUrl(link.url)}>{link.title}</Link></span>
            {link.children.length > 0 && (<ul>
              {link.children.map(link => (
                <li key={link.ID}>
                  <Link href={getPathFromUrl(link.url)}>{link.title}</Link>
                </li>
              ))}              
            </ul>)}
          </li>
        ))}
      </ul>)}
    </div>
  )
}