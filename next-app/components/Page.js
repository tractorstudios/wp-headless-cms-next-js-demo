
import Head from 'next/head'
import Header from './Header'
import Footer from './Footer'
import Sections from './Sections'

export default ({ title, sections, headerLinks, footerLinks, settings }) => (
  <>
    <Head>
      <title>{title}</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <div className="flex flex-col min-h-screen antialiased">
      <Header links={headerLinks} settings={settings} />
      <div className="flex-auto flex flex-col items-stretch justify-start">
        {sections.length > 0 && (
          <>
            {sections.map((section, index) => Sections(section))}
          </>
        )}
        {sections.length == 0 && (
          <div className="p-4 text-red-500">No content specified</div>
        )}
      </div>
      <Footer links={footerLinks} />
    </div>
  </>
)
