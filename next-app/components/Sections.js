import React from 'react'
import Hero from './sections/Hero'
import Content from './sections/Content'
import Contact from './sections/Contact'

const Components = {
  hero: Hero,
  content: Content,
  contact: Contact,
}

export default section => {
  if ( typeof Components[section.acf_fc_layout] !== 'undefined' ) {
    section.id = section.acf_fc_layout
    return React.createElement(Components[section.acf_fc_layout], { ...section })
  }
  return React.createElement(
    () => <div>The section "{section.acf_fc_layout}"" has not been created yet.</div>,
    { ...section }
  )
}