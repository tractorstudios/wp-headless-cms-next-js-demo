import { getPathFromUrl } from '@/lib/utils'
import { useRouter } from 'next/router'
import Link from 'next/link'

export default ({ links, settings }) => {
  const router = useRouter()

  const linkUrl = url => (
    getPathFromUrl(url)
  )

  const linkClass = url => {
    const classes = [ 'ml-4' ]

    if ( `${router.asPath}/` === linkUrl(url) ) {
      classes.push('text-blue-800 font-bold')
    }
    else {
      classes.push('text-blue-500 hover:text-blue-300')
    }

    return classes.join(' ')
  }

  return (
    <div className="flex justify-between items-center p-4">
      <span className="text-gray-500 text-lg"><Link href="/"><span className='cursor-pointer'>{settings.name}</span></Link></span>
      {links && (<nav className="flex text-sm">
        {links.map(link => (
          <span key={link.ID} className={linkClass(link.url)}><Link href={linkUrl(link.url)}>{link.title}</Link></span>
        ))}
      </nav>)}
    </div>
  )
}