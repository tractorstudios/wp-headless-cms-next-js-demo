import { WP_BASE_URL } from '@/lib/constants'
import { useRouter } from 'next/router'

export default ({ id, title, content }) => {
  const router = useRouter()

  const linkHandler = e => {
    if ( e.target.href ) {
      e.preventDefault()
      router.push(e.target.href)
    }
  }

  const cleanUpLinks = html => (
    html.replace(WP_BASE_URL, '')
  )
  
  return (
    <>
      <style jsx>{`

        .content {
        }

        .content :global(h3) {
          font-size: 125%;
          margin-top: 0.5em;
          margin-bottom: 0.5em;
        }

        .content :global(a) {
          color: #00c;
          text-decoration: underline;
        }

      `}</style>
      <section key={id} className="px-4 py-12">
        <h2 className="text-3xl mb-6">{title}</h2>
        <div className="content text-gray-500 max-w-3xl" onClick={linkHandler} dangerouslySetInnerHTML={{ __html: cleanUpLinks(content) }} />
      </section>
    </>
  )
}
