import Image from 'next/image'

export default ({ id, title, subtitle, bg_image }) => (
  <section key={id}  className="px-4 py-32 bg-yellow-400 bg-cover bg-center relative">
    <div className="relative z-10">
      <h1 className="text-5xl mb-2">{title}</h1>
      <h2 className="text-3xl text-white">{subtitle}</h2>
    </div>
    {bg_image?.sizes?.large && (
      <Image src={bg_image.sizes['2048x2048']} layout='fill' objectFit='cover' className='saturate-50 opacity-50' />
    )}
  </section>
)
