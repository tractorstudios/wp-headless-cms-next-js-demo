export default ({ id, title }) => (
  <section key={id} className="px-4 py-12 bg-gray-100">
    <h2 className="text-3xl mb-6">{title}</h2>
    <div className="bg-gray-200 max-w-sm rounded px-4 py-24">
      FPO contact form
    </div>
  </section>
)
