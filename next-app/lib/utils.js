import { PATH_API_URL, MENU_API_URL, PAGES_API_URL, MEDIA_API_URL, SETTINGS_API_URL } from './constants'
import api from '@/lib/api'

const Url = require('url-parse')

export const getAllPages = async () => {
  try {
    const { data } = await api.get(`${PAGES_API_URL}?timestamp=${Date.now()}`)
    return data
  } catch (error) {
    console.log(error)
  }
}

export const getPage = async (id) => {
  try {
    const res = await api.get(`${PAGES_API_URL}/${id}?timestamp=${Date.now()}`)
    return res.data
  } catch (error) {
    console.log(error)
  }
}

export const getPageByPath = async (path) => {
  try {
    const res = await api.get(`${PATH_API_URL}/?path=${path}&timestamp=${Date.now()}`)
    return res.data
  } catch (error) {
    console.log(error)
  }
}

export const getMedia = async (id) => {
  try {
    const res = await api.get(`${MEDIA_API_URL}/${id}?timestamp=${Date.now()}`)
    return res.data.guid.rendered
  } catch (error) {
    console.log(error)
    return ''
  }
}

export const getMenu = async (location) => {
  try {
    const res = await api.get(`${MENU_API_URL}/${location}?timestamp=${Date.now()}`)
    const links = await res.data  
    return links     
  } catch (error) {
    console.log(error)
    return []
  }
}

export const getSettings = async () => {
  try {
    const res = await api.get(`${SETTINGS_API_URL}?timestamp=${Date.now()}`)
    const settings = await res.data  
    return settings
  } catch (error) {
    console.log(error)
    return {}
  }
}

export const getPathFromUrl = (url) => {
  return url ? (new Url(url)).pathname : ''
}