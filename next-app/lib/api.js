import axios from 'axios'
import { WP_BASE_URL } from '@/lib/constants'

const instance = axios.create({
  baseURL: WP_BASE_URL,
})

export default instance

