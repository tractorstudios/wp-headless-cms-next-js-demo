export const PLATFORM           = process.env.PLATFORM ?? 'local'
export const PAGES_API_URL      = process.env.PAGES_API_URL
export const PATH_API_URL       = process.env.PATH_API_URL
export const MENU_API_URL       = process.env.MENU_API_URL
export const MEDIA_API_URL      = process.env.MEDIA_API_URL
export const SETTINGS_API_URL   = process.env.SETTINGS_API_URL
export const WP_BASE_URL        = process.env.NEXT_PUBLIC_WP_BASE_URL