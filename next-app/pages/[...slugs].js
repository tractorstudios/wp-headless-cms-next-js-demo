import Page from '@/components/Page'
import { PLATFORM } from '@/lib/constants'
import { getPathFromUrl, getMenu, getPageByPath, getAllPages, getSettings } from '@/lib/utils'

export const getStaticPaths = async () => {
  
  const pages = await getAllPages()

  const paths = pages.map((page) => {
    return {
      params: { slugs: getPathFromUrl(page.link).split('/').filter(p => p) },
    }
  })

  return { paths, fallback: 'blocking' }
}

export const getStaticProps = async ({ params }) => {

  // page data

  const page = await getPageByPath(params.slugs.join('/'))
  const sections = page.acf?.sections ?? []

  // header menu

  const headerLinks = await getMenu('header') 

  // footer menu

  const footerLinks = await getMenu('footer')     

  // settings

  const settings = await getSettings()

  // platform-specific config
  
  const platformConfig = {}

  if ( PLATFORM === 'vercel' ) {
    platformConfig.revalidate = 5
  }  

  return {
    props: { 
      title: page.title.rendered, 
      sections, 
      headerLinks, 
      footerLinks, 
      settings,
    },
    ...platformConfig,
  }
}

export default Page