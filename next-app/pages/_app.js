import '../styles/styles.scss'
import 'tailwindcss/tailwind.css'

function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default App
