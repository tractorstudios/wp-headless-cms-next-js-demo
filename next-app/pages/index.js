import Page from '@/components/Page'
import { PLATFORM } from '@/lib/constants'
import { getMenu, getSettings, getPageByPath } from '@/lib/utils'

export const getStaticProps = async ({ params }) => {

  // page data
  const page = await getPageByPath('home')
  const sections = page.acf?.sections ?? []

  // header menu

  const headerLinks = await getMenu('header') 

  // footer menu

  const footerLinks = await getMenu('footer')  

  // settings

  const settings = await getSettings()

  // platform-specific config
  
  const platformConfig = {}

  if ( PLATFORM === 'vercel' ) {
    platformConfig.revalidate = 5
  }  

  return {
    props: { 
      title: page.title.rendered, 
      sections, 
      headerLinks, 
      footerLinks, 
      settings,
    },
    ...platformConfig,
  }
}

export default Page