# WordPress Headless CMS + Next.js demo

### WordPress
Fire up a local instance of WordPress at `http://localhost:9009` using Docker
```
cd wordpress
composer install
docker compose up
```

### Next.js
Start Next.js app locally
```
cd next-app
yarn
yarn dev
```