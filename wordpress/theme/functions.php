<?php

// add menu support to theme

add_action( 'after_setup_theme', function() {
    add_theme_support( 'menus' );
    register_nav_menus([
        'header' => 'Header',
        'footer' => 'Footer',
    ]);
});

// register API route to get page by path

add_action('rest_api_init', function () {
	$namespace = 'demo/v1';
	register_rest_route( $namespace, '/page', [
		'methods'  => 'GET',
		'callback' => 'demo_get_page_by_path',
    ]);
});

// method to get page by path

function demo_get_page_by_path(WP_REST_Request $request)
{
    $postId    = get_page_by_path($request->get_param('path'))->ID;
    $postType  = get_post_type($postId);
    $controller = new WP_REST_Posts_Controller($postType);
    $request    = new WP_REST_Request('GET', "/wp/v2/{$postType}s/{$postId}");
    $request->set_url_params([ 'id' => $postId ]);
    return $controller->get_item($request);
}

// include meta fields in page API data

function acf_to_rest_api($response, $page, $request) 
{
    if (!function_exists('get_fields')) return $response;

    if (isset($page)) {
        $acf = get_fields($page->id);
        $response->data['acf'] = $acf;
    }
    return $response;
}
add_filter('rest_prepare_page', 'acf_to_rest_api', 10, 3);